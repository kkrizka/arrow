#
# Package building Arrow as part of the offline / analysis software build.
#

# The name of the package:
atlas_subdir( Arrow )

# In release recompilation mode stop here:
if( ATLAS_RELEASE_MODE )
   return()
endif()

# Tell the user what's happening:
message( STATUS "Building Arrow as part of this project" )

# Temporary directory for the build results:
set( _buildDir ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/ArrowBuild )
# Directory holding the "stamp" files:
set( _stampDir ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/ArrowStamp )

# List of paths given to CMAKE_PREFIX_PATH.
set( _prefixPaths ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM})
#   $ENV{CMAKE_PREFIX_PATH} ${BOOST_LCGROOT} ${Eigen3_DIR} ${nlohmann_json_DIR} )

# Extra configuration options for Arrow:
set( _extraOptions )
if( "${CMAKE_BUILD_TYPE}" STREQUAL "Release" OR
      "${CMAKE_BUILD_TYPE}" STREQUAL "RelWithDebInfo" )
   list( APPEND _extraOptions -DCMAKE_BUILD_TYPE:STRING=Release )
elseif( NOT "${CMAKE_BUILD_TYPE}" STREQUAL "" )
   list( APPEND _extraOptions -DCMAKE_BUILD_TYPE:STRING=${CMAKE_BUILD_TYPE} )
endif()

# Set the repository and tag to be used through cache variables.
set( ATLAS_ARROW_REPOSITORY "https://github.com/apache/arrow.git"
   CACHE STRING "Repository to fetch Arrow from" )
set( ATLAS_ARROW_TAG "apache-arrow-5.0.0"
   CACHE STRING "Git tag to use for the Arrow build" )

# Decide whether / how to patch the Arrow sources.
set( _patchCommand )
# No patches currently needed.

# Build Arrow for the build area:
ExternalProject_Add( Arrow
  PREFIX ${CMAKE_BINARY_DIR}
  INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
  STAMP_DIR ${_stampDir}
  GIT_REPOSITORY "${ATLAS_ARROW_REPOSITORY}"
  GIT_TAG "${ATLAS_ARROW_TAG}"
  SOURCE_SUBDIR cpp
  ${_patchCommand}
  CMAKE_CACHE_ARGS
  -DCMAKE_INSTALL_LIBDIR:PATH=${CMAKE_INSTALL_LIBDIR}
  -DCMAKE_INSTALL_PREFIX:PATH=${_buildDir}
  -DCMAKE_CXX_STANDARD:STRING=${CMAKE_CXX_STANDARD}
  -DCMAKE_CXX_EXTENSIONS:BOOL=${CMAKE_CXX_EXTENSIONS}
  -DARROW_FILESYSTEM:BOOL=ON
  -DARROW_PARQUET:BOOL=ON
  -DARROW_WITH_SNAPPY:BOOL=ON
  -DCMAKE_PREFIX_PATH:PATH=${_prefixPaths}
  ${_extraOptions}
  LOG_CONFIGURE 1 )
ExternalProject_Add_Step( Arrow cleansource
   COMMAND ${CMAKE_COMMAND} -E remove -f
   "${_stampDir}/Arrow-gitclone-lastrun.txt"
   DEPENDERS download )
# Need to modify the printout here when the Arrow version is updated, and we want
# to ensure a clean build.
ExternalProject_Add_Step( Arrow forcedownload
   COMMAND ${CMAKE_COMMAND} -E echo "Forcing the re-download of Arrow (28.06.2021)"
   DEPENDERS cleansource )
ExternalProject_Add_Step( Arrow purgeBuild
   COMMAND ${CMAKE_COMMAND} -E remove_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E make_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E remove_directory "${_buildDir}"
   COMMENT "Removing previous build results for Arrow"
   DEPENDEES download
   DEPENDERS patch )
ExternalProject_Add_Step( Arrow forceconfigure
   COMMAND ${CMAKE_COMMAND} -E remove -f <BINARY_DIR>/CMakeCache.txt
   COMMENT "Forcing the configuration of Arrow"
   DEPENDEES update
   DEPENDERS configure
   ALWAYS 1 )
ExternalProject_Add_Step( Arrow buildinstall
   COMMAND ${CMAKE_COMMAND} -E copy_directory ${_buildDir}/ <INSTALL_DIR>
   COMMENT "Installing Arrow into the build area"
   DEPENDEES install )
add_dependencies( Package_Arrow Arrow )

# And now install it:
install( DIRECTORY ${_buildDir}/
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )
